'use strict';

describe('Controller: HomeCtrl', function() {
  var deferred, myService, $window, $http, $scope, $resource, CompanyService, CompanySearch, AuthService, $location, HomeCtrl, controller, someServiceMock, deferred, authServ;

  beforeEach(module('angularApp.controllers'));

  beforeEach(inject(function(_$window_, _$http_, _$q_, _AuthService_, $rootScope, _$resource_, _CompanyService_, _CompanySearch_, _$location_, $controller) {
     $window = _$window_;
     $http = _$http_;
     $resource = _$resource_;
     AuthService = _AuthService_;
     CompanyService = _CompanyService_;
     CompanySearch = _CompanySearch_;
     $location = _$location_;
     $scope = $rootScope.$new();

     HomeCtrl = $controller('HomeCtrl', {
      $window: $window,
      $http: $http,
      $scope: $scope,
      $resource: $resource,
      CompanyService: CompanyService,
      CompanySearch: CompanySearch,
      AuthService: _AuthService_,
      $location: $location
    });

  }));

  it('call register', function() {
      expect($scope.indexClass).toBe('home');
  });

});
