'use strict';

describe('Service: AuthService', function() {
  var auth, scope, httpBackend, window, user, backend;

  beforeEach(module('angularApp.services'));
  beforeEach(inject(function(_$window_, _$httpBackend_, _backend_, _AuthService_) {
     auth = _AuthService_;
     backend = _backend_;
     httpBackend = _$httpBackend_;
     window = _$window_;
  }));
  it('call login', function(){
     user = {'user': 'lalala', 'password': 'lalala3'}
     httpBackend.expectPOST(backend + '/auth/login/', user).respond({'status':'OK'});
     auth.login(user);
     httpBackend.flush();
  });
  it('call register', function(){
     user = {'user': 'lalala', 'password': 'lalala3'}
     httpBackend.expectPOST(backend + '/auth/register/', user).respond({'status':'OK'});
     auth.register(user);
     httpBackend.flush();
  });
  it('call me', function(){
     user = {'user': 'lalala', 'password': 'lalala3'};
     httpBackend.expectGET(backend + '/auth/me/').respond({'status':'OK'});
     auth.me();
     httpBackend.flush();
  })
});

describe('Service: SubscriptionService', function() {
  var httpBackend, win, backend, company, subscription;

  beforeEach(module('angularApp.services'));
  beforeEach(inject(function(_$window_, _$httpBackend_, _backend_, _SubscriptionService_) {
     backend = _backend_;
     httpBackend = _$httpBackend_;
     win = _$window_;
     subscription = _SubscriptionService_;
  }));
  it('call subscribe', function(){
     company = 1;
     win.sessionStorage.token = '1234';
     httpBackend.expectPUT(backend + '/companies/' + company + '/subscribe').respond({'status':'OK'});
     var response = subscription.subscribe(company);
     httpBackend.flush();
  });
});

describe('Service: CompanySearch', function() {
  var $httpBackend, CompanySearch, resource, backend;

  beforeEach(module('angularApp.services'));
  beforeEach(inject(function(_$resource_, _$httpBackend_, _CompanySearch_, _backend_) {
        resource = _$resource_;
        $httpBackend = _$httpBackend_;
        CompanySearch = _CompanySearch_;
        backend = _backend_;
  }));

  it('call search', function(){
     var getRequest = backend + '/companies';
     $httpBackend.expectGET(getRequest).respond({'status':'OK'});
     CompanySearch.search({}).get();
     $httpBackend.flush();
  });
});
describe('Service: CompanyService', function() {
  var $httpBackend, CompanyService, resource, backend;

  beforeEach(module('angularApp.services'));
  beforeEach(inject(function(_$resource_, _$httpBackend_, _CompanyService_, _backend_) {
        resource = _$resource_;
        $httpBackend = _$httpBackend_;
        CompanyService = _CompanyService_;
        backend = _backend_;
  }));

  it('call get', function(){
     var getRequest = backend + '/companies';
     $httpBackend.expectGET(getRequest).respond({'status':'OK'});
     CompanyService.get();
     $httpBackend.flush();
  });
});
