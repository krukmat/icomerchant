angular.module('angularApp.controllers', ['angularApp.services', 'ngToast', 'ja.qr', 'ui.grid', 'ui.grid.selection', 'ui.grid.cellNav', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ui.grid.pinning'])
.controller('HomeCtrl', function($window, $http, $scope, $resource, CoinbaseAPI, AuthService, $location, uiGridConstants, ngToast, modalService, $timeout) {
   var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            templateUrl: 'partials/modal.html'
        };

     var modalOptions = {
            closeButtonText: 'Close',
            actionButtonText: 'OK',
            headerText: 'Direccion de destino',
            bodyText: 'Ingrese la direccion a enviar y el % neto a enviar (se debe quitar el porcentaje de comision)'
     };

   $scope.indexClass = 'home';
   $scope.coins = [];
   $scope.max_values = [];
   $scope.min_values = [];
   $scope.columns = [{ field: 'amount' }, { field: 'currency' }, { field: 'status' }, {field: 'hash'}];
   $scope.gridOptions = {
    showFooter: false,
    enableSorting: true,
    multiSelect: false,
    enableFiltering: true,
    enableRowSelection: true,
    enableSelectAll: false,
    enableRowHeaderSelection: false,
    enableGridMenu: true,
    noUnselect: true,
    columnDefs: $scope.columns,
    appScopeProvider: {
        onDblClick : function(row) {
           console.log(row.entity);
           modalOptions.comision = 1;
           modalService.showModal(modalDefaults, modalOptions).then(function (result) {
                let destination = result.modalOptions.points;
                let comision = result.modalOptions.comision;
                CoinbaseAPI.confirmTransaction(row.entity.hash, destination, comision).then(function(proxy){
                    ngToast.create({
                        className: 'success',
                        content: 'Transaction sent',
                        dismissButton: true
                     });
                });
           });
         }
    },
    rowTemplate: "<div ng-dblclick=\"grid.appScope.onDblClick(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell></div>"
   };
   $scope.gridOptions.data = [];
   $scope.refresh = true;
   var refresh = function() {
    $scope.refresh = true;
    $timeout(function() {
      $scope.refresh = false;
    }, 0);
  };

  $scope.find_transaction = function() {
       let hash_id = $scope.hash_id;

       CoinbaseAPI.getTransaction(hash_id).then(function(proxy){
           var list = proxy.data[0];
           console.log(list);
           let gridElement = {amount: list.amount.amount, currency: list.amount.currency, status: list.status, hash:list.network.hash};
           $scope.gridOptions.data.push(gridElement);
           refresh();
       });
  }
})
.controller('UserCtrl', function ($rootScope, $scope, $http, $window, $location, AuthService) {
      $scope.user = {email: '', password: ''};
      $scope.isAuthenticated = false;
      $scope.isRegister = false;
      $scope.welcome = '';
      $scope.message = '';

      $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
          console.log(userDetails);
      });

      $scope.selected = 'None';
      $scope.menuOptions = [
          ['Mi cuenta', function ($itemScope) {
              $location.path("/accounts");
          }],
          ['Actualizar Contraseña', function ($itemScope) {
              $location.path("/password");
          }],
          ['Salir', function ($itemScope) {
              $scope.logout();
              $scope.indexClass = '';
              $location.path('/login');
          }]
      ];

      $scope.has_social = function(social) {
          var social_exists = false;
          if ($scope.profile) {
            console.log($scope.profile.social_accounts);
            $scope.profile.social_accounts.forEach(function(social_network) {
                  if (social_network.social === social) {
                      social_exists = true;
                  }
            });
            return social_exists;
          }
          return true;
      }

      $scope.login = function () {
          AuthService.login($scope.user)
          .then(function (data) {
            $window.sessionStorage.token = data.data.auth_token;
            $scope.isAuthenticated = true;
            $scope.indexClass = 'home';
            $location.path("/");
            location.reload();
          }, function (data) {
            // Erase the token if the user fails to log in
            // delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;
            $scope.indexClass = '';

            // Handle login errors here
            $scope.error = data.data.non_field_errors[0];
            $scope.welcome = '';
          });
      };

      $scope.register = function () {
          AuthService.register($scope.user)
          .then(function (data, status, headers, config) {
            $location.path("/login");
          }, function (data) {
            // Erase the token if the user fails to log in
            delete $window.sessionStorage.token;
            $scope.isAuthenticated = false;

            // Handle login errors here
            $scope.error = 'Error: Invalid user or password';
            $scope.welcome = '';
          });
      };

      $scope.logout = function () {
        $scope.welcome = '';
        $scope.message = '';
        $scope.isAuthenticated = false;
        delete $window.sessionStorage.token;
      };

      $scope.callRestricted = function () {
        AuthService.me()
        .then(function (data) {
          $scope.isAuthenticated = true;
          $scope.profile = data.data;
          $scope.indexClass = 'home';
        }, function (data) {
          $scope.isAuthenticated = false;
          $scope.indexClass = '';
          $location.path("/login");
        });
      };
      $scope.callRestricted()

    })
.controller('AccountCtrl', function($scope, AuthService, ngToast) {
  $scope.update = function update(){
      AuthService.update_me($scope.profile);
      ngToast.create({
        className: 'success',
        content: 'Updated',
        dismissButton: true
      });
  }
})
.controller('PasswordCtrl', function($scope, AuthService, ngToast) {
  $scope.passwords = {};
  $scope.passwords.new_password = '';

  $scope.update = function update(){
      AuthService.update_password($scope.passwords);
      ngToast.create({
        className: 'success',
        content: 'Password Updated',
        dismissButton: true
      });
  }
});
