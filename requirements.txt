requests==2.9.1
pytz==2016.3
Django==1.8.8
djangorestframework==3.3.3
django-filter==0.13.0
django-cors-headers==1.1.0
gunicorn==19.1.1
ipython==4.2.0
psycopg2==2.5.4
django-address==0.1.5
django-allauth==0.24.1
django-authtools==1.5.0
gevent==1.1.2
boto==2.1.0
django-ses
coinbase

# Djoser
djoser==0.5.0

# Django model logging
django-auditlog==0.3.3
django-environ==0.4.0

#Test
freezegun==0.3.6
mock==2.0.0
httpio==0.1.3