import freezegun

from mock import Mock, patch, MagicMock

from django.contrib.auth import get_user_model
from django.core import mail

from rest_framework import status
from rest_framework.test import APITestCase


class AuthAPITest(APITestCase):
    @freezegun.freeze_time('2016-01-01', tz_offset=0)
    def setUp(self):
        self.user = get_user_model().objects.create(email='admin@test.com')
        self.user.set_password('testing')
        self.user.save()
        self.data = {
            'email': 'admin@test.com',
            'password': 'testing'
        }

    def test_auth_me_returns_valid_data(self):
        """ Checking /auth/me/ in case of a valid user.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.get('/auth/me/', {})
        self.assertEqual(response.data['email'], 'admin@test.com')

    @patch('django.contrib.auth.tokens.PasswordResetTokenGenerator.check_token',
           MagicMock(return_value=True))
    def test_auth_register_ok_auth_activate_ok(self):
        """ Checking /auth/register/ that creates a valid user profile.
            Then /auth/activate/ and check that returns a valid auth_token for usage
        """
        new_data = {
            'email': 'another_user@test.com',
            'password': 'testing'
        }
        response = self.client.post('/auth/register/', new_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user = get_user_model().objects.get(email='another_user@test.com')
        self.assertEqual(user.is_active, False)
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].subject, 'Account activation on testserver')
        # It logs in and returns a valid token.
        with patch('djoser.utils.decode_uid') as decode:
            decode.return_value = user.pk
            # reset confirm. It's not checked the logic internally. Just checking result 200
            response = self.client.post('/auth/activate/', {
                'uid': user.pk,
                'token': '1234'})
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            # checking password was actually changed
            user = get_user_model().objects.get(pk=user.pk)
            self.assertEqual(user.is_active, True)

    def test_auth_email_returns_ok(self):
        """ Checking /auth/email endpoint.
        This should change the active user's email.
        """
        self.client.force_authenticate(user=self.user)
        response = self.client.post('/auth/email/', {'new_email': 'mail@test.com',
                                                     'current_password': self.data['password']})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check change was done
        self.user = get_user_model().objects.get(pk=self.user.pk)
        self.assertEqual(self.user.email, 'mail@test.com')

    def test_auth_reset_password_valid_email_sent(self):
        """ Checking email sending for password reset.
        """
        response = self.client.post('/auth/password/reset/',
                                    {'email': self.data['email']})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Check email was sent
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(mail.outbox[0].subject, 'Password reset on testserver')

    def test_auth_reset_password_invalid_email_no_email(self):
        """ Checking an invalid email. It should returns 200 but no email sending.
        """
        # Reset password of not existent email should return 200
        response = self.client.post('/auth/password/reset/',
                                    {'email': 'blahbla@notexist.com'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Should not send email
        self.assertEquals(len(mail.outbox), 0)

    @patch('django.contrib.auth.tokens.PasswordResetTokenGenerator.check_token',
           MagicMock(return_value=True))
    def test_auth_reset_password_confirm_returns_ok(self):
        """ Reset password confirm. When user receives the email and clicks on the link it triggers the next.
        In this case it's assumed the uid and token provided are valid (it's beyond the scope of test),
        so the methods decode_uid and check_token are mocked.
        """
        old_password = self.user.password
        # patching decode method to return the unique user id.
        with patch('djoser.utils.decode_uid') as decode:
            decode.return_value = self.user.pk
            # reset confirm. It's not checked the logic internally. Just checking result 200
            response = self.client.post('/auth/password/reset/confirm/', {
                         'uid': self.user.pk,
                         'token': '1234',
                         'new_password': '123456'})
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            # checking password was actually changed
            self.user = get_user_model().objects.get(pk=self.user.pk)
            self.assertNotEqual(old_password, self.user.password)

    def test_auth_logout_valid(self):
        """ Checking when a valid user tries to log out.
        After that token shouldn't be a valid one.
        """
        # self.client.force_authenticate(user=self.user)
        response = self.client.post('/auth/login/', self.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token = response.data['auth_token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        # Logout
        response = self.client.post('/auth/logout/', HTTP_AUTHORIZATION='Token ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Trying to access with the same token (which is invalid)
        response = self.client.get('/auth/me/', HTTP_AUTHORIZATION='Token ' + token)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response.data, {'detail': 'Invalid token.'})