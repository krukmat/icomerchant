# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0019_company_share_company_points'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='shared_rewards',
            field=models.ManyToManyField(to='backend.Reward'),
        ),
    ]