# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0009_company_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('points_redeemed', models.IntegerField(default=0)),
                ('points_pending', models.IntegerField(default=0)),
                ('company', models.ForeignKey(related_name='user_subscriptions', to='backend.Company')),
            ],
        ),
        migrations.RemoveField(
            model_name='mypointsuser',
            name='points_pending',
        ),
        migrations.RemoveField(
            model_name='mypointsuser',
            name='points_redeemed',
        ),
        migrations.AlterField(
            model_name='mypointsuser',
            name='subscriptions',
            field=models.ManyToManyField(to='backend.Subscription'),
        ),
    ]
