# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0014_auto_20170713_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reward',
            name='currency',
            field=models.CharField(max_length=100, null=True, choices=[(b'AR$', b'Peso Argentino'), (b'US$', b'US Dollar'), (b'EUR', b'Euro')]),
        ),
    ]
