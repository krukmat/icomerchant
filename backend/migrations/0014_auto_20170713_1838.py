# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0013_rewardxuser_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reward',
            name='concept',
        ),
        migrations.RemoveField(
            model_name='reward',
            name='conditions',
        ),
        migrations.RemoveField(
            model_name='reward',
            name='infinite',
        ),
        migrations.RemoveField(
            model_name='reward',
            name='is_service',
        ),
    ]
