# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0010_auto_20170323_1917'),
    ]

    operations = [
        migrations.CreateModel(
            name='RewardxUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=255)),
                ('reward', models.ForeignKey(to='backend.Reward')),
                ('user', models.ForeignKey(related_name='rewards', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
