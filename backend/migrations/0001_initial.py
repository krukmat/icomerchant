# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import address.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('address', '0001_initial'),
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='MyPointsUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('email_is_verified', models.BooleanField(default=False)),
                ('is_company', models.BooleanField(default=False)),
                ('is_admin', models.BooleanField(default=False, editable=False)),
                ('is_super_admin', models.BooleanField(default=False, editable=False)),
                ('points_redeemed', models.IntegerField(default=0)),
                ('points_pending', models.IntegerField(default=0)),
                ('offset', models.SmallIntegerField(default=0, verbose_name=b'timezone offset in minutes')),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Category',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name=b'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254)),
                ('website', models.URLField(blank=True)),
                ('facebook', models.URLField(blank=True)),
                ('twitter', models.URLField(blank=True)),
                ('cuit', models.CharField(max_length=30)),
                ('razon_social', models.CharField(max_length=255)),
                ('legales', models.TextField()),
                ('address', address.models.AddressField(to='address.Address')),
                ('category', models.ForeignKey(to='backend.Category')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('user',),
                'verbose_name': 'Company',
                'verbose_name_plural': 'Company',
            },
        ),
        migrations.CreateModel(
            name='Reward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True)),
                ('description', models.TextField(default=b'')),
                ('type', models.CharField(default=b'DISCOUNT', max_length=100, choices=[(b'DISCOUNT', b'Descuento'), (b'VOUCHER', b'Voucher'), (b'PHYSICAL', b'Fisico')])),
                ('currency', models.CharField(max_length=100, choices=[(b'AR$', b'Peso Argentino'), (b'US$', b'US Dollar'), (b'EUR', b'Euro')])),
                ('terms', models.TextField(default=b'')),
                ('points', models.IntegerField(default=0)),
                ('stock', models.IntegerField(default=0)),
                ('period_from', models.DateTimeField(auto_now_add=True)),
                ('period_to', models.DateTimeField(auto_now_add=True)),
                ('conditions', models.TextField(default=b'')),
                ('active', models.BooleanField(default=False)),
                ('codigo', models.CharField(max_length=50)),
                ('infinite', models.BooleanField(default=False)),
                ('concept', models.CharField(default=b'', max_length=255)),
                ('is_service', models.BooleanField(default=False)),
                ('company', models.ForeignKey(to='backend.Company')),
            ],
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='subscriptions',
            field=models.ManyToManyField(to='backend.Company'),
        ),
        migrations.AddField(
            model_name='mypointsuser',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
    ]
