# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0012_auto_20170604_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='rewardxuser',
            name='status',
            field=models.CharField(default=b'PENDIENTE', max_length=50, choices=[(b'PENDIENTE', b'pendiente'), (b'CANCELADO', b'cancelado'), (b'CANJEADO', b'canjeado'), (b'VENCIDO', b'vencido')]),
        ),
    ]
