from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0016_auto_20170713_1903'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='share_reward_points',
            field=models.IntegerField(default=0),
        ),
    ]